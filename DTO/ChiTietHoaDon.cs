﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class ChiTietHoaDon
    {
        public string maHoaDon { get; set; }
        public string maSanPham { get; set; }
        public int soLuong { get; set; }
        public double thanhTienSP { get; set; }
    }
}
