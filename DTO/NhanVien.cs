﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class NhanVien
    {
        public string maNhanVien { get; set; }  
        public string matKhau { get; set; } 
        public string hoTen { get; set; }   
        public int gioiTinh { get; set; }   
        public DateTime ngaySinh { get; set; }

        public string hinhAnh { get; set; }
    }
}
