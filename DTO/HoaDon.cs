﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class HoaDon
    {
        public string maHoaDon { get; set; }
        public string maNhanVien { get; set; }
        public DateTime ngayXuat { get; set; }
        public double thanhTien { get; set; }
        public int banDat { get; set; }
        public string maGiamGia { get; set; }
    }
}
