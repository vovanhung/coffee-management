﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class SanPham
    {
        public string maSanPham { get; set; }
        public string tenSanPham { get; set; }
        public double giaSanPham { get; set; }
    }
}
