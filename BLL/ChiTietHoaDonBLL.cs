﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAL;
using System.Data;

namespace BLL
{
    public class ChiTietHoaDonBLL
    {
        ChiTietHoaDonAccess cthd = new ChiTietHoaDonAccess();

        public DataTable LayChiTietHoaDon(string maHoaDon)
        {
            return cthd.LayChiTiethHoaDon(maHoaDon);
        }

        public bool ThemChiTietHoaDon(string maHoaDon, string maSanPham, int soLuong, double thanhTien)
        {
            return cthd.ThemChiTietHoaDon(maHoaDon,maSanPham,soLuong,thanhTien) >= 1;
        }

        public bool XoaChiTietHoaDon(string maHoaDon, string maSanPham)
        {
            return cthd.XoaSanPham(maHoaDon,maSanPham) >= 1;
        }

        public bool SuaChiTietHoaDon(string maHoaDon, string maSanPham, int soLuong, double thanhTien)
        {
            return cthd.SuaSoLuongSanPham(maHoaDon,maSanPham,soLuong,thanhTien) >= 1;
        }

    }
}
