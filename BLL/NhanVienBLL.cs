﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using DAL;
using System.IO;
namespace BLL
{
    public class NhanVienBLL
    {
        
        private NhanVienAccess nva = new NhanVienAccess();
        public List<NhanVien> LayDanhSachNhanVien()
        {
            return nva.LayDanhSachNhanVien();
        }

        public bool KiemTraDangNhap(string user, string pass)
        {

            return nva.KiemTraDangNhap(user, pass);
        }

        public NhanVien LayNhanVien(string maNhanVien)
        {
            return nva.LayNhanVien(maNhanVien);
        }

        public int ThemNhanVien(string maNhanVien, string matKhau, string hoTen, int gioiTinh, DateTime ngaySinh, string hinhAnh)
        {
            return nva.ThemNhanVien(maNhanVien, matKhau, hoTen, gioiTinh, ngaySinh, hinhAnh);
        }



        public bool SuaNhanVien(string maNhanVien, string matKhau, string hoTen, int gioiTinh, DateTime ngaySinh, string hinhAnh)
        {
            return nva.SuaNhanVien(maNhanVien, matKhau, hoTen, gioiTinh, ngaySinh, hinhAnh) >= 1;
        }

        public bool XoaNhanVien(string maNhanVien)
        {
            return nva.XoaNhanVien(maNhanVien) >= 1;
        }

    }
}
