﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using System.Data;
using System.Data.SqlClient;
using DAL;

namespace BLL
{
    public class HoaDonBLL
    {
        private HoaDonAccess hd = new HoaDonAccess();
        private BanBLL banBLL = new BanBLL();
        private ChiTietHoaDonAccess cthd = new ChiTietHoaDonAccess();

        public DataTable LayDanhSachHoaDon()
        {
            return hd.LayDanhSachHoaDon();
        }

        public DataTable LayDanhSachBAn()
        {
            return banBLL.LayDanhSachBan();
        }

        public bool ThemHoaDon(string maHoaDon, string maNhanVien, DateTime ngayXuat, decimal thanhTien, int maBan)
        {
            return hd.ThemHoaDon(maHoaDon,maNhanVien,ngayXuat,thanhTien,maBan) >= 1;
        }

        public bool ThemChiTietHoaDon(string maHoaDon, string maSanPham, int soLuong, double thanhTien)
        {
            return cthd.ThemChiTietHoaDon(maHoaDon,maSanPham,soLuong,thanhTien) >= 1;
        }

        public DataTable LayChiTietHoaDon(string maHoaDon)
        {
            return cthd.LayChiTiethHoaDon(maHoaDon);
        }

        public bool xoaHoaDon(string maHoaDon)
        {
            return hd.XoaHoaDon(maHoaDon) >= 1;
        }
    }
}
