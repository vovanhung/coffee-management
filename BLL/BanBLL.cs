﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
using System.Data;
using DAL;
namespace BLL
{
    public class BanBLL
    {
        private  BanAccess ban = new BanAccess();

        public DataTable LayDanhSachBan()
        {
            return ban.LayDanhSachBan();
        }

        public bool ThemBan(int maBan, string moTa)
        {
            return ban.ThemBan(maBan, moTa) >= 1;
        }

        public bool XoaBan(int maBan)
        {
            return ban.XoaBan(maBan) >= 1;
        }

        public bool SuaBan(int maBan, string moTa)
        {
            return ban.SuaBan(maBan, moTa) >= 1;
        }
    }
}
