﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DTO;
using DAL;
namespace BLL
{
    public class SanPhamBLL
    {
        SanPhamAccess sp = new SanPhamAccess();

        public DataTable LayDanhSachSanPham()
        {
            return sp.LayDanhSachSanPham();
        }

        public bool ThemSanPham(string maSanPham, string tenSanPham, double giaSanPham)
        {
            return sp.ThemSanPham(maSanPham, tenSanPham, giaSanPham) >= 1;
        }

        public bool XoaSanPham(string maSanPham)
        {
            return sp.XoaSanPham(maSanPham) >= 1;
        }

        public bool SuaSanPham(string maSanPham, string tenSanPham, double giaSanPham)
        {
            return sp.SuaSanPham(maSanPham, tenSanPham, giaSanPham) >= 1;
        }

        public decimal ThanhTienSanPham(string maSP, int soLuong)
        {
            return sp.LayGiaSanPham(maSP) * soLuong;
        }
    }
}
