﻿namespace GUI
{
    partial class frm_HoaDon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_HoaDon));
            this.label1 = new System.Windows.Forms.Label();
            this.cb_ban = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dgv_cacMonDaGoi = new System.Windows.Forms.DataGridView();
            this.MAHOADON_DGV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MASANPHAM_DGV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TENSANPHAM_DGV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SOLUONG_DGV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.THANHTIEN_DGV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_nhapxong = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_xoa = new System.Windows.Forms.Button();
            this.btn_them = new System.Windows.Forms.Button();
            this.txt_soluong = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cb_mon = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_cacMonDaGoi)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 27);
            this.label1.TabIndex = 0;
            this.label1.Text = "Bàn Đặt";
            // 
            // cb_ban
            // 
            this.cb_ban.FormattingEnabled = true;
            this.cb_ban.Location = new System.Drawing.Point(147, 21);
            this.cb_ban.Name = "cb_ban";
            this.cb_ban.Size = new System.Drawing.Size(302, 35);
            this.cb_ban.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(67, 226);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 27);
            this.label2.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(53, 272);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(176, 27);
            this.label3.TabIndex = 3;
            this.label3.Text = "Các Món Đã Gọi";
            // 
            // dgv_cacMonDaGoi
            // 
            this.dgv_cacMonDaGoi.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_cacMonDaGoi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_cacMonDaGoi.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MAHOADON_DGV,
            this.MASANPHAM_DGV,
            this.TENSANPHAM_DGV,
            this.SOLUONG_DGV,
            this.THANHTIEN_DGV});
            this.dgv_cacMonDaGoi.Location = new System.Drawing.Point(58, 302);
            this.dgv_cacMonDaGoi.Name = "dgv_cacMonDaGoi";
            this.dgv_cacMonDaGoi.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_cacMonDaGoi.Size = new System.Drawing.Size(935, 246);
            this.dgv_cacMonDaGoi.TabIndex = 4;
            // 
            // MAHOADON_DGV
            // 
            this.MAHOADON_DGV.HeaderText = "Mã Hóa Đơn";
            this.MAHOADON_DGV.Name = "MAHOADON_DGV";
            // 
            // MASANPHAM_DGV
            // 
            this.MASANPHAM_DGV.HeaderText = "Mã Sản Phẩm";
            this.MASANPHAM_DGV.Name = "MASANPHAM_DGV";
            // 
            // TENSANPHAM_DGV
            // 
            this.TENSANPHAM_DGV.HeaderText = "Tên Sản Phẩm";
            this.TENSANPHAM_DGV.Name = "TENSANPHAM_DGV";
            // 
            // SOLUONG_DGV
            // 
            this.SOLUONG_DGV.HeaderText = "Số Lượng";
            this.SOLUONG_DGV.Name = "SOLUONG_DGV";
            // 
            // THANHTIEN_DGV
            // 
            this.THANHTIEN_DGV.HeaderText = "Thành Tiền";
            this.THANHTIEN_DGV.Name = "THANHTIEN_DGV";
            // 
            // btn_nhapxong
            // 
            this.btn_nhapxong.Image = ((System.Drawing.Image)(resources.GetObject("btn_nhapxong.Image")));
            this.btn_nhapxong.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_nhapxong.Location = new System.Drawing.Point(824, 567);
            this.btn_nhapxong.Name = "btn_nhapxong";
            this.btn_nhapxong.Size = new System.Drawing.Size(169, 48);
            this.btn_nhapxong.TabIndex = 5;
            this.btn_nhapxong.Text = "Nhập Xong";
            this.btn_nhapxong.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_nhapxong.UseVisualStyleBackColor = true;
            this.btn_nhapxong.Click += new System.EventHandler(this.btn_nhapxong_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn_xoa);
            this.groupBox1.Controls.Add(this.btn_them);
            this.groupBox1.Controls.Add(this.txt_soluong);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cb_mon);
            this.groupBox1.Location = new System.Drawing.Point(58, 74);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(854, 186);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thêm Món Ăn";
            // 
            // btn_xoa
            // 
            this.btn_xoa.Image = ((System.Drawing.Image)(resources.GetObject("btn_xoa.Image")));
            this.btn_xoa.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_xoa.Location = new System.Drawing.Point(675, 106);
            this.btn_xoa.Name = "btn_xoa";
            this.btn_xoa.Size = new System.Drawing.Size(121, 48);
            this.btn_xoa.TabIndex = 9;
            this.btn_xoa.Text = "Xóa";
            this.btn_xoa.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_xoa.UseVisualStyleBackColor = true;
            this.btn_xoa.Click += new System.EventHandler(this.btn_xoa_Click);
            // 
            // btn_them
            // 
            this.btn_them.Image = ((System.Drawing.Image)(resources.GetObject("btn_them.Image")));
            this.btn_them.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_them.Location = new System.Drawing.Point(516, 106);
            this.btn_them.Name = "btn_them";
            this.btn_them.Size = new System.Drawing.Size(121, 48);
            this.btn_them.TabIndex = 10;
            this.btn_them.Text = "Thêm";
            this.btn_them.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_them.UseVisualStyleBackColor = true;
            this.btn_them.Click += new System.EventHandler(this.btn_them_Click);
            // 
            // txt_soluong
            // 
            this.txt_soluong.Location = new System.Drawing.Point(166, 119);
            this.txt_soluong.Name = "txt_soluong";
            this.txt_soluong.Size = new System.Drawing.Size(302, 35);
            this.txt_soluong.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(54, 127);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 27);
            this.label5.TabIndex = 7;
            this.label5.Text = "Số Lượng";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(53, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 27);
            this.label4.TabIndex = 5;
            this.label4.Text = "Món";
            // 
            // cb_mon
            // 
            this.cb_mon.FormattingEnabled = true;
            this.cb_mon.Location = new System.Drawing.Point(166, 54);
            this.cb_mon.Name = "cb_mon";
            this.cb_mon.Size = new System.Drawing.Size(302, 35);
            this.cb_mon.TabIndex = 6;
            // 
            // frm_HoaDon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 27F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1033, 618);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btn_nhapxong);
            this.Controls.Add(this.dgv_cacMonDaGoi);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cb_ban);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.Name = "frm_HoaDon";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Nhập Hóa Đơn";
            this.Load += new System.EventHandler(this.frm_HoaDon_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_cacMonDaGoi)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cb_ban;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dgv_cacMonDaGoi;
        private System.Windows.Forms.Button btn_nhapxong;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_xoa;
        private System.Windows.Forms.Button btn_them;
        private System.Windows.Forms.TextBox txt_soluong;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cb_mon;
        private System.Windows.Forms.DataGridViewTextBoxColumn MAHOADON_DGV;
        private System.Windows.Forms.DataGridViewTextBoxColumn MASANPHAM_DGV;
        private System.Windows.Forms.DataGridViewTextBoxColumn TENSANPHAM_DGV;
        private System.Windows.Forms.DataGridViewTextBoxColumn SOLUONG_DGV;
        private System.Windows.Forms.DataGridViewTextBoxColumn THANHTIEN_DGV;
    }
}