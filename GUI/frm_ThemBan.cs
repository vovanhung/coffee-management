﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using DTO;

namespace GUI
{
    public partial class frm_ThemBan : Form
    {
        private BanAn banan;
        private BanBLL banbll = new BanBLL();
        public frm_ThemBan(BanAn banan)
        {
            InitializeComponent();
            this.banan = banan;
        }

        public frm_ThemBan()
        {
            InitializeComponent();
            
        }

        private void LoadDuLieu()
        {
            dgv_themban.DataSource = banbll.LayDanhSachBan();
        }
        private void frm_ThemBan_Load(object sender, EventArgs e)
        {
            LoadDuLieu();
        }

        private void dgv_themban_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            txt_soban.Text = dgv_themban.CurrentRow.Cells["MABAN"].Value.ToString();
            txt_mota.Text = dgv_themban.CurrentRow.Cells["MOTA"].Value.ToString();
        }

        private void btn_them_Click(object sender, EventArgs e)
        {
            try
            {
                if (banbll.ThemBan(int.Parse(txt_soban.Text), txt_mota.Text) == true)
                {
                    MessageBox.Show("Thêm thành công");
                }
                else
                {
                    MessageBox.Show("Thêm thất bại");
                }
                LoadDuLieu();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Violation of PRIMARY KEY"))
                    MessageBox.Show("Mã bàn đã tồn tại");
            }

        }

        private void btn_sua_Click(object sender, EventArgs e)
        {
            if (banbll.SuaBan(int.Parse(txt_soban.Text), txt_mota.Text) == true)
            {
                MessageBox.Show("Sửa thành công");
            }
            else
            {
                MessageBox.Show("Sửa thất bại");
            }
            LoadDuLieu();
        }

        private void btn_xoa_Click(object sender, EventArgs e)
        {
            if (banbll.XoaBan(int.Parse(txt_soban.Text)) == true)
            {
                MessageBox.Show("Xóa thành công");
            }
            else
            {
                MessageBox.Show("Xóa thất bại");
            }
            LoadDuLieu();
        }

        private void btn_dong_Click(object sender, EventArgs e)
        {
            DialogResult kb = MessageBox.Show("Bạn muốn thoát hay không?", "Thoát?", MessageBoxButtons.YesNo);
            if(kb == DialogResult.Yes)
            {
                Close();
            }
        }
    }
}
