﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using DTO;
using GUI.Properties;

namespace GUI
{
    public partial class frm_QuanLyHoaDon : Form
    {
        private NhanVien nhanVien;
        private HoaDonBLL hd = new HoaDonBLL();
        private ChiTietHoaDonBLL cthd = new ChiTietHoaDonBLL();
        public frm_QuanLyHoaDon(NhanVien nhanVien)
        {
            InitializeComponent();
            this.nhanVien = nhanVien;
        }
        public frm_QuanLyHoaDon()
        {
            InitializeComponent();
        }

        private void frm_HoaDon_Load(object sender, EventArgs e)
        {
            LoadDuLieu();
        }

        private void LoadDuLieu()
        {
            dgv_hoadon.DataSource = hd.LayDanhSachHoaDon();
            dgv_chitiethoadon.DataSource = cthd.LayChiTietHoaDon(txt_maHoaDon.Text);
            //txt_tenNhanVien.Text = nhanVien.hoTen;
            //cb_bandat.DataSource = hd.LayDanhSachBAn();
            //cb_bandat.DisplayMember = "MABAN";
            //cb_bandat.ValueMember = "MABAN";
        }

        private void dgv_hoadon_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txt_maHoaDon.Text = dgv_hoadon.CurrentRow.Cells["MAHOADON"].Value.ToString();
            txt_tenNhanVien.Text = dgv_hoadon.CurrentRow.Cells["MANHANVIEN"].Value.ToString();
            txt_thanhtien.Text = dgv_hoadon.CurrentRow.Cells["THANHTIEN"].Value.ToString();
            dt_ngayxuat.Text = dgv_hoadon.CurrentRow.Cells["NGAYXUAT"].Value.ToString();
            txt_BanDat.Text = dgv_hoadon.CurrentRow.Cells["MABAN"].Value.ToString();

            dgv_chitiethoadon.DataSource = hd.LayChiTietHoaDon(txt_maHoaDon.Text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            new frm_HoaDon(DateTime.Now.ToString("ddMMyyyyHHmmss"), nhanVien.maNhanVien).ShowDialog();
        }

        private void frm_QuanLyHoaDon_Activated(object sender, EventArgs e)
        {
            LoadDuLieu();
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Image image = Resources.danang;

            e.Graphics.DrawImage(image, 90, 300, image.Width, image.Height);

            e.Graphics.DrawString("Ngày xuất hóa đơn: " + DateTime.Now.ToShortDateString(), new Font("Arial", 14, FontStyle.Bold), Brushes.Black, new Point(25, 160));

            e.Graphics.DrawString("Mã hóa đơn: " + txt_maHoaDon.Text.Trim(), new Font("Arial", 14, FontStyle.Bold), Brushes.Black, new Point(25, 190));
            e.Graphics.DrawString("Tên nhân viên: " + txt_tenNhanVien.Text.Trim(), new Font("Arial", 14, FontStyle.Bold), Brushes.Black, new Point(25, 220));

            e.Graphics.DrawString("Bàn số: " + txt_BanDat.Text.Trim(), new Font("Arial", 14, FontStyle.Bold), Brushes.Black, new Point(25, 250));

            e.Graphics.DrawString("------------------------------------------------------------------------------------------------------------------------------------", new Font("Arial", 12, FontStyle.Regular), Brushes.Black, new Point(25, 280));

            e.Graphics.DrawString("Tên sản phẩm", new Font("Arial", 14, FontStyle.Bold), Brushes.Black, new Point(30, 300));
            e.Graphics.DrawString("Số lượng", new Font("Arial", 14, FontStyle.Bold), Brushes.Black, new Point(380, 300));
            e.Graphics.DrawString("Đơn giá", new Font("Arial", 14, FontStyle.Bold), Brushes.Black, new Point(510, 300));
            e.Graphics.DrawString("Thành tiền", new Font("Arial", 14, FontStyle.Bold), Brushes.Black, new Point(660, 300));
            e.Graphics.DrawString("------------------------------------------------------------------------------------------------------------------------------------", new Font("Arial", 12, FontStyle.Regular), Brushes.Black, new Point(25, 330));

            int yPos = 350;

           for(int i=0;i<dgv_chitiethoadon.Rows.Count;++i)
            {
                DataGridViewRow datarow = dgv_chitiethoadon.Rows[i];
                
                if (datarow.IsNewRow)
                    continue;
                string tensanpham = datarow.Cells["MAHOADON"].Value.ToString();
                string soluong = datarow.Cells["SOLUONG"].Value.ToString();
                string dongia = datarow.Cells["GIASP"].Value.ToString();
                string thanhtien = datarow.Cells["THANHTIENSP"].Value.ToString();
                e.Graphics.DrawString(tensanpham, new Font("Arial", 14, FontStyle.Bold), Brushes.Black, new Point(30, yPos));
                e.Graphics.DrawString(soluong.ToString(), new Font("Arial", 14, FontStyle.Bold), Brushes.Black, new Point(400, yPos));
                e.Graphics.DrawString(dongia.ToString(), new Font("Arial", 14, FontStyle.Bold), Brushes.Black, new Point(525, yPos));
                e.Graphics.DrawString(thanhtien.ToString(), new Font("Arial", 14, FontStyle.Bold), Brushes.Black, new Point(675, yPos));
                yPos += 30;
            }
              
            e.Graphics.DrawString("------------------------------------------------------------------------------------------------------------------------------------", new Font("Arial", 12, FontStyle.Regular), Brushes.Black, new Point(25, yPos));

            e.Graphics.DrawString("Tổng tiền:      " + txt_thanhtien.Text.Trim() + "₫", new Font("Arial", 14, FontStyle.Bold), Brushes.Black, new Point(550, yPos + 30));
           
        }

        private void button5_Click(object sender, EventArgs e)
        {
            printPreviewDialog1.Document = printDocument1;
            printPreviewDialog1.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Ban Muốn Thoát Chương Trình", "Thông Báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialog == DialogResult.Yes)
            {
                Close();
            }
        }

        private void btn_xoa_Click(object sender, EventArgs e)
        {
            if(hd.xoaHoaDon(txt_maHoaDon.Text))
            {
                MessageBox.Show("Xóa thành công");
            }
            else
            {
                MessageBox.Show("Xóa thất bại");
            }
        }
    }
}
