﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BLL;

namespace GUI
{
    public partial class frm_DangNhap : Form
    {
        public string user;
        public frm_DangNhap()
        {
            InitializeComponent();
        }
       
        private void frm_DangNhap_Load(object sender, EventArgs e)
        {
            
        }

        private void btn_dangnhap_Click(object sender, EventArgs e)
        {
            
            NhanVienBLL nhanVienBLL = new NhanVienBLL();
            string user = txt_user.Text;    
            string pass = txt_pass.Text;
            if(user=="")
            {
                MessageBox.Show("Tài khoản không được để trống");
            }
            else if(pass=="")
            {
                MessageBox.Show("Mật khẩu không được để trống");
            }
            else
            {
                if (!nhanVienBLL.KiemTraDangNhap(user, pass))
                {
                    MessageBox.Show("Sai tên đăng nhập / mật khẩu");
                }
                else
                {
                    this.Hide();
                    Main kb = new Main(txt_user.Text);
                    MessageBox.Show("Xin chào  " + user);
                    kb.Show();
                }
            }
     
        }

        private void txt_pass_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
