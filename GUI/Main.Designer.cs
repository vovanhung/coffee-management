﻿namespace GUI
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            DevExpress.XtraEditors.TileItemElement tileItemElement1 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement2 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement3 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame1 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement4 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement5 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement6 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame2 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement7 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement8 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement9 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement10 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement11 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement12 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement13 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement14 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement15 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame3 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement16 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement17 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement18 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemFrame tileItemFrame4 = new DevExpress.XtraEditors.TileItemFrame();
            DevExpress.XtraEditors.TileItemElement tileItemElement19 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement20 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement21 = new DevExpress.XtraEditors.TileItemElement();
            DevExpress.XtraEditors.TileItemElement tileItemElement22 = new DevExpress.XtraEditors.TileItemElement();
            this.tileControl1 = new DevExpress.XtraEditors.TileControl();
            this.tileGroup4 = new DevExpress.XtraEditors.TileGroup();
            this.tl_quanlymonan = new DevExpress.XtraEditors.TileItem();
            this.tl_themmon = new DevExpress.XtraEditors.TileItem();
            this.tl_thembanan = new DevExpress.XtraEditors.TileItem();
            this.tileGroup5 = new DevExpress.XtraEditors.TileGroup();
            this.tileItem7 = new DevExpress.XtraEditors.TileItem();
            this.tl_themthanhvien = new DevExpress.XtraEditors.TileItem();
            this.SuspendLayout();
            // 
            // tileControl1
            // 
            this.tileControl1.AppearanceGroupText.BorderColor = System.Drawing.Color.White;
            this.tileControl1.AppearanceGroupText.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tileControl1.AppearanceGroupText.ForeColor = System.Drawing.Color.White;
            this.tileControl1.AppearanceGroupText.Options.UseBorderColor = true;
            this.tileControl1.AppearanceGroupText.Options.UseFont = true;
            this.tileControl1.AppearanceGroupText.Options.UseForeColor = true;
            this.tileControl1.AppearanceText.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tileControl1.AppearanceText.Options.UseFont = true;
            this.tileControl1.BackColor = System.Drawing.Color.Honeydew;
            this.tileControl1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("tileControl1.BackgroundImage")));
            this.tileControl1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tileControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tileControl1.Groups.Add(this.tileGroup4);
            this.tileControl1.Groups.Add(this.tileGroup5);
            this.tileControl1.Location = new System.Drawing.Point(0, 0);
            this.tileControl1.Margin = new System.Windows.Forms.Padding(4);
            this.tileControl1.MaxId = 11;
            this.tileControl1.Name = "tileControl1";
            this.tileControl1.Padding = new System.Windows.Forms.Padding(27, 26, 27, 26);
            this.tileControl1.ShowGroupText = true;
            this.tileControl1.ShowText = true;
            this.tileControl1.Size = new System.Drawing.Size(906, 515);
            this.tileControl1.TabIndex = 2;
            this.tileControl1.Click += new System.EventHandler(this.tileControl1_Click_1);
            // 
            // tileGroup4
            // 
            this.tileGroup4.Items.Add(this.tl_quanlymonan);
            this.tileGroup4.Items.Add(this.tl_themmon);
            this.tileGroup4.Items.Add(this.tl_thembanan);
            this.tileGroup4.Name = "tileGroup4";
            this.tileGroup4.Text = "Quản lý món ăn";
            // 
            // tl_quanlymonan
            // 
            tileItemElement1.Appearance.Hovered.Font = new System.Drawing.Font("Segoe UI Light", 17F);
            tileItemElement1.Appearance.Hovered.Options.UseFont = true;
            tileItemElement1.Appearance.Hovered.Options.UseTextOptions = true;
            tileItemElement1.Appearance.Hovered.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement1.Appearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement1.Appearance.Normal.Font = new System.Drawing.Font("Segoe UI Light", 17F);
            tileItemElement1.Appearance.Normal.Options.UseFont = true;
            tileItemElement1.Appearance.Normal.Options.UseTextOptions = true;
            tileItemElement1.Appearance.Normal.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement1.Appearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement1.Appearance.Selected.Font = new System.Drawing.Font("Segoe UI Light", 17F);
            tileItemElement1.Appearance.Selected.Options.UseFont = true;
            tileItemElement1.Appearance.Selected.Options.UseTextOptions = true;
            tileItemElement1.Appearance.Selected.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement1.Appearance.Selected.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement1.MaxWidth = 160;
            tileItemElement1.Text = "Đặt món";
            tileItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileItemElement1.TextLocation = new System.Drawing.Point(75, 0);
            tileItemElement2.Appearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement2.Appearance.Hovered.Options.UseFont = true;
            tileItemElement2.Appearance.Hovered.Options.UseTextOptions = true;
            tileItemElement2.Appearance.Hovered.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement2.Appearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            tileItemElement2.Appearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement2.Appearance.Normal.Options.UseFont = true;
            tileItemElement2.Appearance.Normal.Options.UseTextOptions = true;
            tileItemElement2.Appearance.Normal.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement2.Appearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            tileItemElement2.Appearance.Selected.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement2.Appearance.Selected.Options.UseFont = true;
            tileItemElement2.Appearance.Selected.Options.UseTextOptions = true;
            tileItemElement2.Appearance.Selected.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement2.Appearance.Selected.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            tileItemElement2.MaxWidth = 160;
            tileItemElement2.Text = "Đặt các món ăn";
            tileItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileItemElement2.TextLocation = new System.Drawing.Point(75, 30);
            tileItemElement3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            tileItemElement3.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileItemElement3.ImageOptions.ImageLocation = new System.Drawing.Point(4, 8);
            tileItemElement3.ImageOptions.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomOutside;
            tileItemElement3.ImageOptions.ImageSize = new System.Drawing.Size(64, 64);
            this.tl_quanlymonan.Elements.Add(tileItemElement1);
            this.tl_quanlymonan.Elements.Add(tileItemElement2);
            this.tl_quanlymonan.Elements.Add(tileItemElement3);
            tileItemFrame1.Animation = DevExpress.XtraEditors.TileItemContentAnimationType.ScrollTop;
            tileItemElement4.Appearance.Hovered.Font = new System.Drawing.Font("Segoe UI Light", 17F);
            tileItemElement4.Appearance.Hovered.Options.UseFont = true;
            tileItemElement4.Appearance.Hovered.Options.UseTextOptions = true;
            tileItemElement4.Appearance.Hovered.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement4.Appearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement4.Appearance.Normal.Font = new System.Drawing.Font("Segoe UI Light", 17F);
            tileItemElement4.Appearance.Normal.Options.UseFont = true;
            tileItemElement4.Appearance.Normal.Options.UseTextOptions = true;
            tileItemElement4.Appearance.Normal.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement4.Appearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement4.Appearance.Selected.Font = new System.Drawing.Font("Segoe UI Light", 17F);
            tileItemElement4.Appearance.Selected.Options.UseFont = true;
            tileItemElement4.Appearance.Selected.Options.UseTextOptions = true;
            tileItemElement4.Appearance.Selected.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement4.Appearance.Selected.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement4.MaxWidth = 160;
            tileItemElement4.Text = "Đặt món";
            tileItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileItemElement4.TextLocation = new System.Drawing.Point(75, 0);
            tileItemElement5.Appearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement5.Appearance.Hovered.Options.UseFont = true;
            tileItemElement5.Appearance.Hovered.Options.UseTextOptions = true;
            tileItemElement5.Appearance.Hovered.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement5.Appearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            tileItemElement5.Appearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement5.Appearance.Normal.Options.UseFont = true;
            tileItemElement5.Appearance.Normal.Options.UseTextOptions = true;
            tileItemElement5.Appearance.Normal.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement5.Appearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            tileItemElement5.Appearance.Selected.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement5.Appearance.Selected.Options.UseFont = true;
            tileItemElement5.Appearance.Selected.Options.UseTextOptions = true;
            tileItemElement5.Appearance.Selected.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement5.Appearance.Selected.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            tileItemElement5.MaxWidth = 160;
            tileItemElement5.Text = "Đặt các món ăn";
            tileItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileItemElement5.TextLocation = new System.Drawing.Point(75, 30);
            tileItemElement6.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            tileItemElement6.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileItemElement6.ImageOptions.ImageLocation = new System.Drawing.Point(4, 8);
            tileItemElement6.ImageOptions.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomOutside;
            tileItemElement6.ImageOptions.ImageSize = new System.Drawing.Size(64, 64);
            tileItemFrame1.Elements.Add(tileItemElement4);
            tileItemFrame1.Elements.Add(tileItemElement5);
            tileItemFrame1.Elements.Add(tileItemElement6);
            tileItemElement7.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            tileItemElement7.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement7.ImageOptions.ImageLocation = new System.Drawing.Point(4, 8);
            tileItemElement7.ImageOptions.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomOutside;
            tileItemElement7.ImageOptions.ImageSize = new System.Drawing.Size(64, 64);
            tileItemFrame2.Elements.Add(tileItemElement7);
            tileItemFrame2.Image = ((System.Drawing.Image)(resources.GetObject("tileItemFrame2.Image")));
            this.tl_quanlymonan.Frames.Add(tileItemFrame1);
            this.tl_quanlymonan.Frames.Add(tileItemFrame2);
            this.tl_quanlymonan.Id = 0;
            this.tl_quanlymonan.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide;
            this.tl_quanlymonan.Name = "tl_quanlymonan";
            this.tl_quanlymonan.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tl_quanlymonan_ItemClick);
            // 
            // tl_themmon
            // 
            tileItemElement8.Appearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement8.Appearance.Hovered.Options.UseFont = true;
            tileItemElement8.Appearance.Hovered.Options.UseTextOptions = true;
            tileItemElement8.Appearance.Hovered.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement8.Appearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            tileItemElement8.Appearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement8.Appearance.Normal.Options.UseFont = true;
            tileItemElement8.Appearance.Normal.Options.UseTextOptions = true;
            tileItemElement8.Appearance.Normal.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement8.Appearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            tileItemElement8.Appearance.Selected.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement8.Appearance.Selected.Options.UseFont = true;
            tileItemElement8.Appearance.Selected.Options.UseTextOptions = true;
            tileItemElement8.Appearance.Selected.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement8.Appearance.Selected.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            tileItemElement8.MaxWidth = 200;
            tileItemElement8.Text = "Thêm món ăn vào danh sách";
            tileItemElement8.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileItemElement8.TextLocation = new System.Drawing.Point(4, 11);
            tileItemElement9.Appearance.Hovered.Font = new System.Drawing.Font("Segoe UI Light", 49F);
            tileItemElement9.Appearance.Hovered.Options.UseFont = true;
            tileItemElement9.Appearance.Hovered.Options.UseTextOptions = true;
            tileItemElement9.Appearance.Hovered.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            tileItemElement9.Appearance.Hovered.TextOptions.Trimming = DevExpress.Utils.Trimming.Character;
            tileItemElement9.Appearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement9.Appearance.Normal.Font = new System.Drawing.Font("Segoe UI Light", 49F);
            tileItemElement9.Appearance.Normal.Options.UseFont = true;
            tileItemElement9.Appearance.Normal.Options.UseTextOptions = true;
            tileItemElement9.Appearance.Normal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            tileItemElement9.Appearance.Normal.TextOptions.Trimming = DevExpress.Utils.Trimming.Character;
            tileItemElement9.Appearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement9.Appearance.Selected.Font = new System.Drawing.Font("Segoe UI Light", 49F);
            tileItemElement9.Appearance.Selected.Options.UseFont = true;
            tileItemElement9.Appearance.Selected.Options.UseTextOptions = true;
            tileItemElement9.Appearance.Selected.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            tileItemElement9.Appearance.Selected.TextOptions.Trimming = DevExpress.Utils.Trimming.Character;
            tileItemElement9.Appearance.Selected.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement9.MaxWidth = 100;
            tileItemElement9.Text = "1";
            tileItemElement9.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopRight;
            tileItemElement9.TextLocation = new System.Drawing.Point(0, -15);
            tileItemElement10.Appearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement10.Appearance.Hovered.Options.UseFont = true;
            tileItemElement10.Appearance.Hovered.Options.UseTextOptions = true;
            tileItemElement10.Appearance.Hovered.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            tileItemElement10.Appearance.Hovered.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement10.Appearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement10.Appearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement10.Appearance.Normal.Options.UseFont = true;
            tileItemElement10.Appearance.Normal.Options.UseTextOptions = true;
            tileItemElement10.Appearance.Normal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            tileItemElement10.Appearance.Normal.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement10.Appearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement10.Appearance.Selected.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement10.Appearance.Selected.Options.UseFont = true;
            tileItemElement10.Appearance.Selected.Options.UseTextOptions = true;
            tileItemElement10.Appearance.Selected.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            tileItemElement10.Appearance.Selected.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement10.Appearance.Selected.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement10.MaxWidth = 70;
            tileItemElement10.Text = "Quản lý";
            tileItemElement10.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleRight;
            tileItemElement10.TextLocation = new System.Drawing.Point(-3, 15);
            this.tl_themmon.Elements.Add(tileItemElement8);
            this.tl_themmon.Elements.Add(tileItemElement9);
            this.tl_themmon.Elements.Add(tileItemElement10);
            this.tl_themmon.Id = 2;
            this.tl_themmon.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide;
            this.tl_themmon.Name = "tl_themmon";
            this.tl_themmon.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tl_themmon_ItemClick_1);
            // 
            // tl_thembanan
            // 
            tileItemElement11.Appearance.Hovered.Font = new System.Drawing.Font("Segoe UI Light", 49F);
            tileItemElement11.Appearance.Hovered.Options.UseFont = true;
            tileItemElement11.Appearance.Hovered.Options.UseTextOptions = true;
            tileItemElement11.Appearance.Hovered.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            tileItemElement11.Appearance.Hovered.TextOptions.Trimming = DevExpress.Utils.Trimming.Character;
            tileItemElement11.Appearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement11.Appearance.Normal.Font = new System.Drawing.Font("Segoe UI Light", 49F);
            tileItemElement11.Appearance.Normal.Options.UseFont = true;
            tileItemElement11.Appearance.Normal.Options.UseTextOptions = true;
            tileItemElement11.Appearance.Normal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            tileItemElement11.Appearance.Normal.TextOptions.Trimming = DevExpress.Utils.Trimming.Character;
            tileItemElement11.Appearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement11.Appearance.Selected.Font = new System.Drawing.Font("Segoe UI Light", 49F);
            tileItemElement11.Appearance.Selected.Options.UseFont = true;
            tileItemElement11.Appearance.Selected.Options.UseTextOptions = true;
            tileItemElement11.Appearance.Selected.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            tileItemElement11.Appearance.Selected.TextOptions.Trimming = DevExpress.Utils.Trimming.Character;
            tileItemElement11.Appearance.Selected.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement11.MaxWidth = 100;
            tileItemElement11.Text = "3";
            tileItemElement11.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileItemElement11.TextLocation = new System.Drawing.Point(2, -15);
            tileItemElement12.Appearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement12.Appearance.Hovered.Options.UseFont = true;
            tileItemElement12.Appearance.Hovered.Options.UseTextOptions = true;
            tileItemElement12.Appearance.Hovered.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            tileItemElement12.Appearance.Hovered.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement12.Appearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement12.Appearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement12.Appearance.Normal.Options.UseFont = true;
            tileItemElement12.Appearance.Normal.Options.UseTextOptions = true;
            tileItemElement12.Appearance.Normal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            tileItemElement12.Appearance.Normal.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement12.Appearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement12.Appearance.Selected.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement12.Appearance.Selected.Options.UseFont = true;
            tileItemElement12.Appearance.Selected.Options.UseTextOptions = true;
            tileItemElement12.Appearance.Selected.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            tileItemElement12.Appearance.Selected.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement12.Appearance.Selected.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement12.MaxWidth = 140;
            tileItemElement12.Text = "Thêm bàn ăn";
            tileItemElement12.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleLeft;
            tileItemElement12.TextLocation = new System.Drawing.Point(3, 15);
            this.tl_thembanan.Elements.Add(tileItemElement11);
            this.tl_thembanan.Elements.Add(tileItemElement12);
            this.tl_thembanan.Id = 10;
            this.tl_thembanan.ItemSize = DevExpress.XtraEditors.TileItemSize.Medium;
            this.tl_thembanan.Name = "tl_thembanan";
            this.tl_thembanan.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tl_thembanan_ItemClick);
            // 
            // tileGroup5
            // 
            this.tileGroup5.Items.Add(this.tileItem7);
            this.tileGroup5.Items.Add(this.tl_themthanhvien);
            this.tileGroup5.Name = "tileGroup5";
            this.tileGroup5.Text = "Thành viên và thống kê";
            // 
            // tileItem7
            // 
            tileItemElement13.Appearance.Hovered.Font = new System.Drawing.Font("Segoe UI Light", 17F);
            tileItemElement13.Appearance.Hovered.Options.UseFont = true;
            tileItemElement13.Appearance.Hovered.Options.UseTextOptions = true;
            tileItemElement13.Appearance.Hovered.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement13.Appearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement13.Appearance.Normal.Font = new System.Drawing.Font("Segoe UI Light", 17F);
            tileItemElement13.Appearance.Normal.Options.UseFont = true;
            tileItemElement13.Appearance.Normal.Options.UseTextOptions = true;
            tileItemElement13.Appearance.Normal.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement13.Appearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement13.Appearance.Selected.Font = new System.Drawing.Font("Segoe UI Light", 17F);
            tileItemElement13.Appearance.Selected.Options.UseFont = true;
            tileItemElement13.Appearance.Selected.Options.UseTextOptions = true;
            tileItemElement13.Appearance.Selected.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement13.Appearance.Selected.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement13.MaxWidth = 160;
            tileItemElement13.Text = "Thống kê";
            tileItemElement13.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileItemElement13.TextLocation = new System.Drawing.Point(75, 0);
            tileItemElement14.Appearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement14.Appearance.Hovered.Options.UseFont = true;
            tileItemElement14.Appearance.Hovered.Options.UseTextOptions = true;
            tileItemElement14.Appearance.Hovered.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement14.Appearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            tileItemElement14.Appearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement14.Appearance.Normal.Options.UseFont = true;
            tileItemElement14.Appearance.Normal.Options.UseTextOptions = true;
            tileItemElement14.Appearance.Normal.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement14.Appearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            tileItemElement14.Appearance.Selected.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement14.Appearance.Selected.Options.UseFont = true;
            tileItemElement14.Appearance.Selected.Options.UseTextOptions = true;
            tileItemElement14.Appearance.Selected.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement14.Appearance.Selected.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            tileItemElement14.MaxWidth = 160;
            tileItemElement14.Text = "Thống kê theo tháng";
            tileItemElement14.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileItemElement14.TextLocation = new System.Drawing.Point(75, 30);
            tileItemElement15.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image3")));
            tileItemElement15.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileItemElement15.ImageOptions.ImageBorderColor = System.Drawing.Color.White;
            tileItemElement15.ImageOptions.ImageLocation = new System.Drawing.Point(4, 8);
            tileItemElement15.ImageOptions.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomOutside;
            tileItemElement15.ImageOptions.ImageSize = new System.Drawing.Size(64, 64);
            this.tileItem7.Elements.Add(tileItemElement13);
            this.tileItem7.Elements.Add(tileItemElement14);
            this.tileItem7.Elements.Add(tileItemElement15);
            tileItemFrame3.Animation = DevExpress.XtraEditors.TileItemContentAnimationType.ScrollTop;
            tileItemElement16.Appearance.Hovered.Font = new System.Drawing.Font("Segoe UI Light", 17F);
            tileItemElement16.Appearance.Hovered.Options.UseFont = true;
            tileItemElement16.Appearance.Hovered.Options.UseTextOptions = true;
            tileItemElement16.Appearance.Hovered.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement16.Appearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement16.Appearance.Normal.Font = new System.Drawing.Font("Segoe UI Light", 17F);
            tileItemElement16.Appearance.Normal.Options.UseFont = true;
            tileItemElement16.Appearance.Normal.Options.UseTextOptions = true;
            tileItemElement16.Appearance.Normal.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement16.Appearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement16.Appearance.Selected.Font = new System.Drawing.Font("Segoe UI Light", 17F);
            tileItemElement16.Appearance.Selected.Options.UseFont = true;
            tileItemElement16.Appearance.Selected.Options.UseTextOptions = true;
            tileItemElement16.Appearance.Selected.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement16.Appearance.Selected.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement16.MaxWidth = 160;
            tileItemElement16.Text = "Thống kê";
            tileItemElement16.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileItemElement16.TextLocation = new System.Drawing.Point(75, 0);
            tileItemElement17.Appearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement17.Appearance.Hovered.Options.UseFont = true;
            tileItemElement17.Appearance.Hovered.Options.UseTextOptions = true;
            tileItemElement17.Appearance.Hovered.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement17.Appearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            tileItemElement17.Appearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement17.Appearance.Normal.Options.UseFont = true;
            tileItemElement17.Appearance.Normal.Options.UseTextOptions = true;
            tileItemElement17.Appearance.Normal.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement17.Appearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            tileItemElement17.Appearance.Selected.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement17.Appearance.Selected.Options.UseFont = true;
            tileItemElement17.Appearance.Selected.Options.UseTextOptions = true;
            tileItemElement17.Appearance.Selected.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement17.Appearance.Selected.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            tileItemElement17.MaxWidth = 160;
            tileItemElement17.Text = "Thống kê theo tháng";
            tileItemElement17.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileItemElement17.TextLocation = new System.Drawing.Point(75, 30);
            tileItemElement18.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image4")));
            tileItemElement18.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileItemElement18.ImageOptions.ImageBorderColor = System.Drawing.Color.White;
            tileItemElement18.ImageOptions.ImageLocation = new System.Drawing.Point(4, 8);
            tileItemElement18.ImageOptions.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomOutside;
            tileItemElement18.ImageOptions.ImageSize = new System.Drawing.Size(64, 64);
            tileItemFrame3.Elements.Add(tileItemElement16);
            tileItemFrame3.Elements.Add(tileItemElement17);
            tileItemFrame3.Elements.Add(tileItemElement18);
            tileItemFrame4.Animation = DevExpress.XtraEditors.TileItemContentAnimationType.ScrollDown;
            tileItemElement19.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("resource.Image5")));
            tileItemElement19.ImageOptions.ImageAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileItemElement19.ImageOptions.ImageBorderColor = System.Drawing.Color.White;
            tileItemElement19.ImageOptions.ImageLocation = new System.Drawing.Point(4, 8);
            tileItemElement19.ImageOptions.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.ZoomOutside;
            tileItemElement19.ImageOptions.ImageSize = new System.Drawing.Size(64, 64);
            tileItemFrame4.Elements.Add(tileItemElement19);
            tileItemFrame4.Image = ((System.Drawing.Image)(resources.GetObject("tileItemFrame4.Image")));
            this.tileItem7.Frames.Add(tileItemFrame3);
            this.tileItem7.Frames.Add(tileItemFrame4);
            this.tileItem7.Id = 4;
            this.tileItem7.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide;
            this.tileItem7.Name = "tileItem7";
            this.tileItem7.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tileItem7_ItemClick);
            // 
            // tl_themthanhvien
            // 
            tileItemElement20.Appearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement20.Appearance.Hovered.Options.UseFont = true;
            tileItemElement20.Appearance.Hovered.Options.UseTextOptions = true;
            tileItemElement20.Appearance.Hovered.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement20.Appearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            tileItemElement20.Appearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement20.Appearance.Normal.Options.UseFont = true;
            tileItemElement20.Appearance.Normal.Options.UseTextOptions = true;
            tileItemElement20.Appearance.Normal.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement20.Appearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            tileItemElement20.Appearance.Selected.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement20.Appearance.Selected.Options.UseFont = true;
            tileItemElement20.Appearance.Selected.Options.UseTextOptions = true;
            tileItemElement20.Appearance.Selected.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement20.Appearance.Selected.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            tileItemElement20.MaxWidth = 145;
            tileItemElement20.Text = "Thêm thành viên";
            tileItemElement20.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.Manual;
            tileItemElement20.TextLocation = new System.Drawing.Point(4, 11);
            tileItemElement21.Appearance.Hovered.Font = new System.Drawing.Font("Segoe UI Light", 49F);
            tileItemElement21.Appearance.Hovered.Options.UseFont = true;
            tileItemElement21.Appearance.Hovered.Options.UseTextOptions = true;
            tileItemElement21.Appearance.Hovered.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            tileItemElement21.Appearance.Hovered.TextOptions.Trimming = DevExpress.Utils.Trimming.Character;
            tileItemElement21.Appearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement21.Appearance.Normal.Font = new System.Drawing.Font("Segoe UI Light", 49F);
            tileItemElement21.Appearance.Normal.Options.UseFont = true;
            tileItemElement21.Appearance.Normal.Options.UseTextOptions = true;
            tileItemElement21.Appearance.Normal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            tileItemElement21.Appearance.Normal.TextOptions.Trimming = DevExpress.Utils.Trimming.Character;
            tileItemElement21.Appearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement21.Appearance.Selected.Font = new System.Drawing.Font("Segoe UI Light", 49F);
            tileItemElement21.Appearance.Selected.Options.UseFont = true;
            tileItemElement21.Appearance.Selected.Options.UseTextOptions = true;
            tileItemElement21.Appearance.Selected.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            tileItemElement21.Appearance.Selected.TextOptions.Trimming = DevExpress.Utils.Trimming.Character;
            tileItemElement21.Appearance.Selected.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement21.MaxWidth = 100;
            tileItemElement21.Text = "2";
            tileItemElement21.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopRight;
            tileItemElement21.TextLocation = new System.Drawing.Point(0, -15);
            tileItemElement22.Appearance.Hovered.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement22.Appearance.Hovered.Options.UseFont = true;
            tileItemElement22.Appearance.Hovered.Options.UseTextOptions = true;
            tileItemElement22.Appearance.Hovered.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            tileItemElement22.Appearance.Hovered.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement22.Appearance.Hovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement22.Appearance.Normal.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement22.Appearance.Normal.Options.UseFont = true;
            tileItemElement22.Appearance.Normal.Options.UseTextOptions = true;
            tileItemElement22.Appearance.Normal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            tileItemElement22.Appearance.Normal.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement22.Appearance.Normal.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement22.Appearance.Selected.Font = new System.Drawing.Font("Segoe UI", 9F);
            tileItemElement22.Appearance.Selected.Options.UseFont = true;
            tileItemElement22.Appearance.Selected.Options.UseTextOptions = true;
            tileItemElement22.Appearance.Selected.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            tileItemElement22.Appearance.Selected.TextOptions.Trimming = DevExpress.Utils.Trimming.EllipsisCharacter;
            tileItemElement22.Appearance.Selected.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            tileItemElement22.MaxWidth = 70;
            tileItemElement22.Text = "Quản lý";
            tileItemElement22.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleRight;
            tileItemElement22.TextLocation = new System.Drawing.Point(-3, 15);
            this.tl_themthanhvien.Elements.Add(tileItemElement20);
            this.tl_themthanhvien.Elements.Add(tileItemElement21);
            this.tl_themthanhvien.Elements.Add(tileItemElement22);
            this.tl_themthanhvien.Id = 1;
            this.tl_themthanhvien.ItemSize = DevExpress.XtraEditors.TileItemSize.Wide;
            this.tl_themthanhvien.Name = "tl_themthanhvien";
            this.tl_themthanhvien.ItemClick += new DevExpress.XtraEditors.TileItemClickEventHandler(this.tl_themthanhvien_ItemClick);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(906, 515);
            this.Controls.Add(this.tileControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Main";
            this.Text = "Main";
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.TileControl tileControl1;
        private DevExpress.XtraEditors.TileGroup tileGroup4;
        private DevExpress.XtraEditors.TileItem tl_quanlymonan;
        private DevExpress.XtraEditors.TileItem tl_themmon;
        private DevExpress.XtraEditors.TileItem tl_thembanan;
        private DevExpress.XtraEditors.TileGroup tileGroup5;
        private DevExpress.XtraEditors.TileItem tileItem7;
        private DevExpress.XtraEditors.TileItem tl_themthanhvien;
    }
}