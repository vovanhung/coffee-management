﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using DTO;

namespace GUI
{
    public partial class frm_ThemNhanVien : Form
    {
        public string duongdan = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName + @"\Hinh\";
        private NhanVien nhanvien;
        private NhanVienBLL nhanVienBLL = new NhanVienBLL();
       
        public frm_ThemNhanVien()
        {
            InitializeComponent();
        }

        public frm_ThemNhanVien(NhanVien nhanvien)
        {
            InitializeComponent();
            this.nhanvien = nhanvien;
        }

        private void LoadDuLieu()
        {
            dgv_nhanvien.DataSource = nhanVienBLL.LayDanhSachNhanVien();
        }
        private void frm_ThemNhanVien_Load(object sender, EventArgs e)
        {
            LoadDuLieu();
        }

        private void dgv_nhanvien_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            txt_manhanvien.Text = dgv_nhanvien.CurrentRow.Cells["MANHANVIEN"].Value.ToString();
            txt_matkhau.Text = dgv_nhanvien.CurrentRow.Cells["MATKHAU"].Value.ToString();
            txt_hoten.Text = dgv_nhanvien.CurrentRow.Cells["HOTEN"].Value.ToString();
            cb_GioiTinh.Text = dgv_nhanvien.CurrentRow.Cells["GIOITINH"].Value.ToString();
            dtp_ngaysinh.Text = dgv_nhanvien.CurrentRow.Cells["NGAYSINH"].Value.ToString();
            txt_hinhanh.Text = dgv_nhanvien.CurrentRow.Cells["HINHANH"].Value.ToString();
            pb_hinhanh.ImageLocation = duongdan + txt_hinhanh.Text;
        }

        private void btn_them_Click(object sender, EventArgs e)
        {
            try
            {
                if (nhanVienBLL.ThemNhanVien(txt_manhanvien.Text, txt_matkhau.Text, txt_hoten.Text, int.Parse(cb_GioiTinh.Text), dtp_ngaysinh.Value, txt_hinhanh.Text) == 1)
                {
                    MessageBox.Show("Thêm thành công");
                    pb_hinhanh.Image.Save(duongdan + txt_hinhanh.Text);
                }
                else
                {
                    MessageBox.Show("Thêm thất bại");
                }
                LoadDuLieu();
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Violation of PRIMARY KEY"))
                    MessageBox.Show("Mã nhân viên đã tồn tại");
            }
            
        }

        private void btn_sua_Click(object sender, EventArgs e)
        {
            if (nhanVienBLL.SuaNhanVien(txt_manhanvien.Text, txt_matkhau.Text, txt_hoten.Text, int.Parse( cb_GioiTinh.Text), dtp_ngaysinh.Value, txt_hinhanh.Text) == true)
            {
                MessageBox.Show("Sửa thành công");
                pb_hinhanh.Image.Save(duongdan + txt_hinhanh.Text);
            }
            else
            {
                MessageBox.Show("Sửa thất bại");
            }
            LoadDuLieu();
        }

        private void btn_Xóa_Click(object sender, EventArgs e)
        {
            if (nhanVienBLL.XoaNhanVien(txt_manhanvien.Text) == true)
            {
                MessageBox.Show("Xóa thành công");
                File.Delete(duongdan + txt_hinhanh.Text);
            }
            else
            {
                MessageBox.Show("Xóa thất bại");
            }
            LoadDuLieu();
        }

        private void btn_Dong_Click(object sender, EventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Ban Muốn Thoát Chương Trình", "Thông Báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dialog == DialogResult.Yes)
            {
                Close();
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openanh = new OpenFileDialog();
            openanh.Title = "Mời bạn chọn ảnh";
            openanh.Filter = "BMP file|*.JPG|JEPG file|*.JEPG|All file|*.*";
            if (openanh.ShowDialog() == DialogResult.OK)
                try
                {
                    pb_hinhanh.Image = Image.FromFile(openanh.FileName);
                }
                catch (OutOfMemoryException)
                {
                    MessageBox.Show("Định dạng ảnh không đúng");
                }
        }
    }
}
