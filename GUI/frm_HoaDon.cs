﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BLL;
using System.Globalization;

namespace GUI
{
    public partial class frm_HoaDon : Form
    {
        private string MaHoaDon;
        private string MaNV;
        private HoaDonBLL hd = new HoaDonBLL();
        private SanPhamBLL sp = new SanPhamBLL();
        public frm_HoaDon()
        {
            InitializeComponent();
        }

        public frm_HoaDon(string maHoaDon, string maNV)
        {
            this.MaHoaDon = maHoaDon;
            this.MaNV = maNV;
            InitializeComponent();
        }

        private void frm_HoaDon_Load(object sender, EventArgs e)
        {

            cb_ban.DataSource = hd.LayDanhSachBAn();
            cb_ban.DisplayMember = "MABAN";
            cb_ban.ValueMember = "MABAN";

            cb_mon.DataSource = sp.LayDanhSachSanPham();
            cb_mon.DisplayMember = "TENSANPHAM";
            cb_mon.ValueMember = "MASANPHAM";

        }

        private void Them(string maSP, int soLuong)
        {
            foreach (DataGridViewRow row in dgv_cacMonDaGoi.Rows)
            {
                if (row.Cells["MASANPHAM_DGV"].Value!= null && row.Cells["MASANPHAM_DGV"].Value.Equals(maSP))
                {
                    soLuong += (int)row.Cells["SOLUONG_DGV"].Value;
                    dgv_cacMonDaGoi.Rows.Remove(row);
                    break;
                }
            }
            dgv_cacMonDaGoi.Rows.Add(MaHoaDon, maSP, cb_mon.Text, soLuong, sp.ThanhTienSanPham(maSP, soLuong));
            dgv_cacMonDaGoi.Refresh();
        }

        private void btn_them_Click(object sender, EventArgs e)
        {
            string maSP = cb_mon.SelectedValue.ToString();
            int soluong = int.Parse(txt_soluong.Text);
            Them(maSP, soluong);

        }

        private void btn_xoa_Click(object sender, EventArgs e)
        {
            DataGridViewRow row = dgv_cacMonDaGoi.CurrentRow;
            dgv_cacMonDaGoi.Rows.Remove(row);
        }

        private void btn_nhapxong_Click(object sender, EventArgs e)
        {
            decimal thanhTien = 0;
            foreach (DataGridViewRow row in dgv_cacMonDaGoi.Rows)
            {
                if (row.Cells["THANHTIEN_DGV"].Value != null)
                    thanhTien+=(decimal) row.Cells["THANHTIEN_DGV"].Value;
            }
            DateTime ngayXuat = DateTime.Now;
            bool saveHoaDon = hd.ThemHoaDon(MaHoaDon, MaNV, ngayXuat, thanhTien, int.Parse(cb_ban.SelectedValue.ToString()));
            bool saveChiTietHoaDon = true;
            foreach (DataGridViewRow row in dgv_cacMonDaGoi.Rows)
            {
                if (row.Cells["MAHOADON_DGV"].Value == null) break;
                string maHoaDon = row.Cells["MAHOADON_DGV"].Value.ToString();
                string maSP= row.Cells["MASANPHAM_DGV"].Value.ToString(); 
                int soLuongSP = int.Parse(row.Cells["SOLUONG_DGV"].Value.ToString());
                double thanhTienSP = double.Parse(row.Cells["THANHTIEN_DGV"].Value.ToString());
                saveChiTietHoaDon = saveChiTietHoaDon && hd.ThemChiTietHoaDon(maHoaDon, maSP, soLuongSP, thanhTienSP);
            }

            if (saveHoaDon && saveChiTietHoaDon)
            {
                this.Close();
            }
            else
            {
                MessageBox.Show("Lỗi Khi Thêm Hóa Đơn Mới");
            }  
        }
    }
}
