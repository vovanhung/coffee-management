﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
namespace GUI
{
    public partial class frm_thongke : Form
    {
        public HoaDonBLL hoadonbll = new HoaDonBLL();
        public frm_thongke()
        {
            InitializeComponent();
        }

        public void loaddulieu()
        {
            dgv_thongke.DataSource = hoadonbll.LayDanhSachHoaDon();
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void frm_thongke_Load(object sender, EventArgs e)
        {
            loaddulieu();
            thongke();
        }

        public void thongke()
        {
            int sc = dgv_thongke.Rows.Count;
            Double thanhtien = 0;
            for (int i = 0; i < sc - 1; i++)
            {
                thanhtien += float.Parse(dgv_thongke.Rows[i].Cells["THANHTIEN"].Value.ToString());
            }
            txt_tongtien.Text = thanhtien.ToString();

        }
    }
}
