﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DTO;
using BLL;
namespace GUI
{
    public partial class frm_ThemSanPham : Form
    {
        private SanPham SanPham;
        private SanPhamBLL sanPhamBLL = new SanPhamBLL();
      
        private frm_ThemSanPham(SanPham sanPham)
        {
            InitializeComponent();
            this.SanPham = SanPham;
        }

        public frm_ThemSanPham()
        {
            InitializeComponent();
            
        }

        private void frm_ThemSanPham_Load(object sender, EventArgs e)
        {
            Loaddulieu();
        }

        private void Loaddulieu()
        {
            dgv_themsanpham.DataSource = sanPhamBLL.LayDanhSachSanPham();
        }

        private void dgv_themsanpham_CellClick(object sender, DataGridViewCellEventArgs e)
        {
           
        }

        private void dgv_themsanpham_CellClick_1(object sender, DataGridViewCellEventArgs e)
        {
            txt_masanpham.Text = dgv_themsanpham.CurrentRow.Cells["MASANPHAM"].Value.ToString();
            txt_tensanpham.Text = dgv_themsanpham.CurrentRow.Cells["TENSANPHAM"].Value.ToString();
            txt_giasanpham.Text = dgv_themsanpham.CurrentRow.Cells["GIASP"].Value.ToString();
        }

        private void btn_them_Click(object sender, EventArgs e)
        {
           
     
        }

        private void btn_them_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (sanPhamBLL.ThemSanPham(txt_masanpham.Text, txt_tensanpham.Text, double.Parse(txt_giasanpham.Text)) == true)
                {
                    MessageBox.Show("Thêm thành công");
                }
                else
                {
                    MessageBox.Show("Thêm thất bại");
                }
                Loaddulieu();
            }
            catch(Exception ex)
            {
                if (ex.Message.Contains("Violation of PRIMARY KEY"))
                    MessageBox.Show("Mã sản phẩm đã tồn tại");
            }
            
            

        }

        private void btn_sua_Click(object sender, EventArgs e)
        {
            if(sanPhamBLL.SuaSanPham(txt_masanpham.Text, txt_tensanpham.Text, double.Parse(txt_giasanpham.Text))==true)
            {
                MessageBox.Show("Sửa thành công");
            }
            else
            {
                MessageBox.Show("Sửa thất bại");
            }
            Loaddulieu();
        }

        private void btn_xoa_Click(object sender, EventArgs e)
        {
            if (sanPhamBLL.XoaSanPham(txt_masanpham.Text) == true)
            {
                MessageBox.Show("Xóa thành công");
            }
            else
            {
                MessageBox.Show("Xóa thất bại");
            }
            Loaddulieu();
        }

        private void btn_dong_Click(object sender, EventArgs e)
        {
            DialogResult kb = MessageBox.Show("Bạn muốn thoát hay không?", "Thoát", MessageBoxButtons.YesNo);
            
            if(kb == DialogResult.Yes)
            {
               Close();
            }
            
        }
    }
}
