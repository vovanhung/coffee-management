﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using BLL;
using DTO;

namespace GUI
{
    public partial class Main : DevExpress.XtraEditors.XtraForm
    {
        private NhanVienBLL nhanVienBLL = new NhanVienBLL();
        string user1;
        public Main()
        {
            InitializeComponent();
        }
        public Main(string user)
        {
            InitializeComponent();
            user1 = user;

        }

        private void tl_themmon_ItemClick(object sender, TileItemEventArgs e)
        {
          
        }

        private void tl_themthanhvien_ItemClick(object sender, TileItemEventArgs e)
        {
            frm_ThemNhanVien nv = new frm_ThemNhanVien();
            nv.ShowDialog();
        }

        private void tl_thembanan_ItemClick(object sender, TileItemEventArgs e)
        {
            frm_ThemBan tb = new frm_ThemBan();
            tb.ShowDialog();
        }

        private void tl_themmon_ItemClick_1(object sender, TileItemEventArgs e)
        {
            frm_ThemSanPham kb = new frm_ThemSanPham();
            kb.ShowDialog();
        }

        private void tl_quanlymonan_ItemClick(object sender, TileItemEventArgs e)
        {
            new frm_QuanLyHoaDon(nhanVienBLL.LayNhanVien(user1)).Show();
        }

        private void tileControl1_Click(object sender, EventArgs e)
        {

        }

        private void tileControl1_Click_1(object sender, EventArgs e)
        {

        }

        private void tileItem7_ItemClick(object sender, TileItemEventArgs e)
        {
            frm_thongke tk = new frm_thongke();
            tk.ShowDialog();
        }
    }
}