﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class DatabaseAccess
    {
        string strConn = @"Data Source=DESKTOP-DOV76SP\SQLEXPRESS;Initial Catalog=DOANNHOM;Integrated Security=True";
        protected SqlConnection conn = null;

        public void OpenConnection()
        {
            if (conn == null)
            {
                conn = new SqlConnection(strConn);
            }

            if (conn.State == ConnectionState.Closed)
            {
                conn.Open();
            }
        }

        public void CloseConnection()
        {
            if (conn != null && conn.State == ConnectionState.Open)
            {
                conn.Close();
            }
        }

        protected DataTable GetDataTable(string sql)
        {
            OpenConnection();
            SqlDataAdapter da = new SqlDataAdapter(sql, conn);
            DataTable dt = new DataTable();
            da.Fill(dt);
            CloseConnection();
            return dt;
        }

        protected int ExecuteNonQuery(string sql)
        {
            OpenConnection();
            SqlCommand comm = new SqlCommand(sql, conn);
            int result = (int)comm.ExecuteNonQuery();
            CloseConnection();
            return result;
        
        }

    }
}
