﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace DAL
{
    public class ChiTietHoaDonAccess :DatabaseAccess
    {
        public DataTable LayChiTiethHoaDon(string mahoadon)
        {
            string sql = "select SP.TENSANPHAM, CTHD.SOLUONG, SP.GIASP, CTHD.THANHTIENSP from CHITIETHOADON CTHD, SANPHAM SP where CTHD.MASANPHAM = SP.MASANPHAM AND MAHOADON = '"+mahoadon+"'";
            return GetDataTable(sql);
        }

        public int ThemChiTietHoaDon(string maHoaDon, string maSanPham, int soLuong, double thanhTien)
        {
            string insert = "insert into CHITIETHOADON values ('" + maHoaDon+"','"+maSanPham+"','"+soLuong+"','"+thanhTien+"')";
            return ExecuteNonQuery(insert);
        }
        
        public int XoaSanPham(string maHoaDon, string maSanPham)
        {
            string delete = "delete from CHITIETHOADON where MAHOADON = '"+maHoaDon+"' AND  MASANPHAM = '"+maSanPham+"'";
            return ExecuteNonQuery(delete);
        }

        public int SuaSoLuongSanPham(string maHoaDon, string maSanPham, int soLuong, double thanhTien)
        {
            string update = "update CHITIETHOADON set SOLUONG = '"+soLuong+"', THANHTIENSP = '"+thanhTien+"'  where MAHOADON = '"+maHoaDon+ "'AND  MASANPHAM = '" + maSanPham + "' ";
            return ExecuteNonQuery(update);
        }
    }
}
