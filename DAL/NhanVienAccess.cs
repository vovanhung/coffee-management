﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTO;
namespace DAL
{
    public class NhanVienAccess : DatabaseAccess
    {
        public bool KiemTraDangNhap(string user, string pass)
        {
            OpenConnection();
            //string strCommand = @"SELECT COUNT(*) FROM NHANVIEN WHERE  MANHANVIEN = '" + user + "' AND MATKHAU = '" + pass + "'";
            //SqlCommand com = new SqlCommand(strCommand, conn);
            string strCommand = @"SELECT COUNT(*) FROM NHANVIEN WHERE  MANHANVIEN = @user AND MATKHAU = @pass";
            SqlCommand com = new SqlCommand(strCommand, conn);
            com.Parameters.Add(new SqlParameter("user", user));
            com.Parameters.Add(new SqlParameter("pass", pass));

            int result = (int) com.ExecuteScalar();          
            return result > 0;
        }

        public List<NhanVien> LayDanhSachNhanVien()
        {
            List<NhanVien> dsNV = new List<NhanVien>();
            OpenConnection();
            string strCommand = "SELECT * FROM NHANVIEN";
            SqlCommand com = new SqlCommand(strCommand, conn);
            SqlDataReader reader = com.ExecuteReader();
            while (reader.Read())
            {
                string maNhanVienS = reader.GetString(0);
                string matKhauS = reader.GetString(1);
                string hoTenS = reader.GetString(2);
                int gioiTinhS = reader.GetInt32(3);
                DateTime ngaySinhS = reader.GetDateTime(4);
                string hinhAnhS = reader.GetString(5);

                NhanVien nv = new NhanVien
                {
                    maNhanVien = maNhanVienS,
                    matKhau = matKhauS,
                    hoTen = hoTenS,
                    gioiTinh = gioiTinhS,
                    ngaySinh = ngaySinhS,
                    hinhAnh = hinhAnhS
                };
                dsNV.Add(nv);

            }
            reader.Close();
            return dsNV;
            
        }

        public NhanVien LayNhanVien(string maNhanVien)
        {
            OpenConnection();
            string strCommand = "SELECT * FROM NHANVIEN where MANHANVIEN = @maNhanVien";
            SqlCommand com = new SqlCommand(strCommand, conn);
            com.Parameters.Add(new SqlParameter("maNhanVien", maNhanVien));
            SqlDataReader reader = com.ExecuteReader();
            NhanVien nv = new NhanVien();
            while (reader.Read())
            {
                string maNhanVienS = reader.GetString(0);
                string matKhauS = reader.GetString(1);
                string hoTenS = reader.GetString(2);
                int gioiTinhS = reader.GetInt32(3);
                DateTime ngaySinhS = reader.GetDateTime(4);
                string hinhAnhS = reader.GetString(5);

                nv.maNhanVien = maNhanVienS;
                nv.matKhau = maNhanVienS;
                nv.hoTen = hoTenS;
                nv.gioiTinh = gioiTinhS;
                nv.ngaySinh = ngaySinhS;
                nv.hinhAnh = hinhAnhS;

            }
            reader.Close();
            return nv;

        }

        public int ThemNhanVien(string maNhanVien, string matKhau, string hoTen, int gioiTinh, DateTime ngaySinh, string hinhAnh)
        {
            string insert = "INSERT into NHANVIEN VALUES ('" + maNhanVien + "','" + matKhau + "','" + hoTen + "','" + gioiTinh + "',convert(DateTime, '" + ngaySinh + "', 103),'" + hinhAnh+"')";
            return ExecuteNonQuery(insert);
        }

        public int XoaNhanVien(string maNhanVien)
        {
            string delete = "delete NHANVIEN where MANHANVIEN = '" + maNhanVien + "'";
            return ExecuteNonQuery(delete);
        }
        public int SuaNhanVien(string maNhanVien, string matKhau, string hoTen, int gioiTinh, DateTime ngaySinh, string hinhAnh)
        {
            string update = "update NHANVIEN set  MATKHAU = '" + matKhau + "' , GIOITINH = '" + gioiTinh + "', NGAYSINH = '" + ngaySinh +"', HINHANH = '"+ hinhAnh + "' where MANHANVIEN = '" + maNhanVien + "' ";
            return ExecuteNonQuery(update);
        }
    }
}
