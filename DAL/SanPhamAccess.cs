﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
namespace DAL
{
    public class SanPhamAccess : DatabaseAccess
    {
        public DataTable LayDanhSachSanPham()
        {
            return GetDataTable("Select * from SANPHAM");
        }

        public int ThemSanPham(string MASANPHAM, string TENSANPHAM, double GIASP)
        {
            string insert = "insert into SANPHAM values ('" + MASANPHAM + "', '" + TENSANPHAM + "', '" + GIASP + "')";
            return ExecuteNonQuery(insert);
        }
        public int XoaSanPham(string MASANPHAM)
        {
            string delete = "delete SANPHAM where MASANPHAM = '" + MASANPHAM + "'";
            return ExecuteNonQuery(delete);
        }
        public int SuaSanPham(string MASANPHAM, string TENSANPHAM, double GIASP)
        {
            string update = "update SANPHAM set TENSANPHAM = '" + TENSANPHAM + "', GIASP = '"+GIASP+"'  where MASANPHAM = '" + MASANPHAM + "' ";
            return ExecuteNonQuery(update);
        }

        public decimal LayGiaSanPham(string maSP)
        {
            string selectGiaSP = "select GIASP from SANPHAM where MASANPHAM = '"+maSP+"'";
            OpenConnection();
            SqlCommand com = new SqlCommand(selectGiaSP, conn);
            SqlDataReader rd = com.ExecuteReader();
            decimal res = 1;
            while (rd.Read())
            {
                res = rd.GetDecimal(0); 
            }
             
            CloseConnection();
            return res;
        }

        
    }
}
