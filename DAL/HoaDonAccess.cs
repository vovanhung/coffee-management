﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class HoaDonAccess : DatabaseAccess
    {
        public DataTable LayDanhSachHoaDon()
        {
            return GetDataTable("select * from HOADON");
        }

        public int ThemHoaDon(string maHoaDon, string maNhanVien, DateTime ngayXuat, decimal thanhTien, int maBan)
        {
            string insert = "INSERT into HOADON VALUES ('" + maHoaDon+"','"+maNhanVien+"',convert(DateTime, '"+ngayXuat+"', 103),'"+thanhTien+"','"+maBan+"') ";
            return ExecuteNonQuery(insert);
        }

        public int XoaHoaDon(string maHoaDon)
        {
            string delete = "delete HOADON where MAHOADON = '" + maHoaDon + "'";
            return ExecuteNonQuery(delete);
        }
        public int SuaHoaDon(string maHoaDon, string maNhanVien, DateTime ngayXuat, decimal thanhTien, int maBan)
        {
            string update = "update HOADON set NGAYXUAT = '" + ngayXuat + "', THANHTIEN = '" + thanhTien + "'  where MAHOADON = '" + maHoaDon + "' ";
            return ExecuteNonQuery(update);
        }
    }
}
